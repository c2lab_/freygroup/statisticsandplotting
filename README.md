# Statistics and plotting
This repository contains the following:
- **models_final_update.R**: the code to estimate our regression results.
- **Figure1_0419.R**: the code to produce Figure1.
- **Figure2_0419.R**: the code to produce Figure2.
- **Figure4_0419.R**: the code to produce the current Figure3.

## Instruction
To run the code, you'll need to go through the following steps:
1.  Clone this repo to your local address.
```sh
$ git clone https://gitlab.com/c2lab_/freygroup/statisticsandplotting.git
```
That creates a directory named statisticasandplotting in your local address, initializes a .git directory inside it, pulls down all the data for that repository, and checks out a working copy of the latest version. If you go into the new learningtogovern directory that was just created, you’ll see the project files in there, ready to be worked on or used.

2. Then download the stats_clean_data/ folder from https://ucdavis.box.com/s/bdwzish6booxgnwbi9tl3b6pnsrh98r1 to your local statisticasandplotting directory.
3. Run each R file to produce result.


## Data
- **rule_count_cov.csv**: non-temporal community-level rule count data with all covariants
- **rule_count_age.csv**: non-temporal community-level rule count data among 3 domains with covariant age
